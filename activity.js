  {
      "first_name": "Niñonez Deo",
      "last_name": "Olor",
      "email": "ninonezdeoolor11@gmail.com",
      "password": "password123",
      "is_admin": true,
      "mobile_number": "+1-234-567-8910"
  },

  {
      "user_id": 1,
      "transaction_date": "2023-03-27T23:30:00+08:00",
      "status": "pending",
      "total": 20000
  },

  {
      "user_id": 1,
      "transaction_date": "2023-03-27T23:35:00+08:00",
      "status": "completed",
      "total": 30000
  },

  {
      "user_id": 1,
      "transaction_date": "2023-03-27T23:40:00+08:00",
      "status": "pending",
      "total": 40000
  },

  {
      "user_id": 1,
      "transaction_date": "2023-03-27T23:45:00+08:00",
      "status": "completed",
      "total": 50000
  },

  {
      "name": "Alienware",
      "description": "Alienware Laptop",
      "price": 10000,
      "stocks": 5,
      "is_active": true,
      "sku": "AW01"
  },

  {
      "name": "ROG",
      "description": "ROG Laptop",
      "price": 10000,
      "stocks": 10,
      "is_active": true,
      "sku": "ROG02"
  },

  {
      "name": "Predator",
      "description": "Predator Laptop",
      "price": 10000,
      "stocks": 15,
      "is_active": true,
      "sku": "ROG02"
  },

    {
      "name": "Omen",
      "description": "Omen Laptop",
      "price": 10000,
      "stocks": 20,
      "is_active": true,
      "sku": "ROG02"
  },

  {
      "order_id": 1,
      "product_id": 1,
      "quantity": 2,
      "price": 10000,
      "sub_total": 20000
  },

  {
      "order_id": 1,
      "product_id": 2,
      "quantity": 3,
      "price": 10000,
      "sub_total": 30000
  },

  {
      "order_id": 2,
      "product_id": 3,
      "quantity": 4,
      "price": 10000,
      "sub_total": 40000
  },

  {
      "order_id": 2,
      "product_id": 4,
      "quantity": 5,
      "price": 10000,
      "sub_total": 50000
  }